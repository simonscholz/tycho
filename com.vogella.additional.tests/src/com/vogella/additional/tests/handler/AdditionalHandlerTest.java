package com.vogella.additional.tests.handler;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.Status;
import org.junit.Test;

import com.vogella.additional.handler.AdditionalHandler;

public class AdditionalHandlerTest {

	@Test
	public void testAdditionalHandler() {
		AdditionalHandler additionalHandler = new AdditionalHandler();
		try {
			Object execute = additionalHandler.execute(null);
			assertEquals(Status.OK_STATUS, execute);
		} catch (ExecutionException e) {
			fail(e.getMessage());
		}
	}
}
