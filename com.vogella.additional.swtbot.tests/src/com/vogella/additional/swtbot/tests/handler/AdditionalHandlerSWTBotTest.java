package com.vogella.additional.swtbot.tests.handler;

import static org.junit.Assert.assertNotNull;

import org.eclipse.swtbot.swt.finder.SWTBot;
import org.eclipse.swtbot.swt.finder.junit.SWTBotJunit4ClassRunner;
import org.eclipse.swtbot.swt.finder.widgets.SWTBotToolbarButton;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(SWTBotJunit4ClassRunner.class)
public class AdditionalHandlerSWTBotTest {

	private SWTBot bot;
	
	@Before
	public void setUp() {
		bot = new SWTBot();
	}

	@Test
	public void testAdditional() {
		
		SWTBotToolbarButton toolbarButton = bot.toolbarButtonWithTooltip("Hello my friend");
		assertNotNull(toolbarButton);
		toolbarButton.click();
	}
}
