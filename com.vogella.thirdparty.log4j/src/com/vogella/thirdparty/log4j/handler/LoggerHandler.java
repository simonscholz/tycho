package com.vogella.thirdparty.log4j.handler;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.runtime.Status;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggerHandler extends AbstractHandler implements IHandler {

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		Logger logger = LoggerFactory.getLogger(getClass());
		
		logger.info("Just using an osgified slf4j logger.");
		
		return Status.OK_STATUS;
	}

}
